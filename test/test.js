// greybox testing
var expect = require('chai').expect,
    app = require('../testapp/test-server'),
    api = require('supertest')(app),

    // npm will copy the current extension version on test
    extension = require('../testapp/node_modules/dpd-user-require-old-password/index'),

    testuser = {
        username: 'test@user.net', 
        password: 'testing'
    };
var userStore = app.createStore('users');

before(function(done){
    this.timeout(10000);

    process.server.on('listening', function() {
        // delete existing user
        userStore.remove({username: testuser.username}, function() {
            // setup a testuser to use with the next requests
            api.post('/users')
                .send(testuser)
                .expect(200)
                .end(function(error, res) {
                    expect(error).to.equal(null);

                    testuser.id = res.body.id;

                    api.post('/users/login')
                        .send({username: testuser.username, password: testuser.password})
                        .expect(200)
                        .end(function(error, res) {
                            expect(error).to.equal(null);

                            testuser.sid = res.body.id;

                            done();
                        });
                });
        });
    }); // wait for the server to startup
});

describe('dpd-user-require-old-password', function(){

    it('should require oldPassword', function(done){
        api.post('/users/login')
            .send({username: testuser.username, password: testuser.password})
            .expect(200)
            .end(function(error, res){

                expect(res.body)
                    .to.have.property('uid');
                expect(res.body)
                    .to.have.property('id');

                api.put('/users/'+res.body.uid)
                    .set('Cookie', 'sid='+res.body.id)
                    .send({password: 'newPassword'})
                    .expect(400)
                    .end(function(error, res){

                        expect(res.body)
                            .to.have.property('errors')
                            .with.property('oldPassword');
                        done(error);
                    });
            });
    });

    it('should check oldPassword', function(done){
        api.post('/users/login')
            .send({username: testuser.username, password: testuser.password})
            .expect(200)
            .end(function(error, res){

                expect(res.body)
                    .to.have.property('uid');

                api.put('/users/'+res.body.uid)
                    .set('Cookie', 'sid='+res.body.id)
                    .send({password: 'newPassword', oldPassword: 'invalid'})
                    .expect(400)
                    .end(function(error, res){
                        expect(res.body)
                            .to.have.property('errors')
                            .with.property('oldPassword');
                        done(error);
                    });
            });
    });

    it('should succeed with correct oldPassword', function(done){
        api.post('/users/login')
            .send({username: testuser.username, password: testuser.password})
            .expect(200)
            .end(function(error, res){
                expect(res.body)
                    .to.have.property('uid');

                api.put('/users/'+res.body.uid)
                    .set('Cookie', 'sid='+res.body.id)
                    .send({password: 'newPassword', oldPassword: testuser.password})
                    .expect(200)
                    .end(function(error, res){

                        expect(res.body)
                            .to.not.have.property('errors');
                        done(error);
                    });
            });
    });
});
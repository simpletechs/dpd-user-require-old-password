var UserCollection = require('deployd/lib/resources/user-collection'),
    debug = require('debug')('dpd-user-require-old-password'),
    _handle = UserCollection.prototype.handle; // might break if other plugins do the same, but probably not

UserCollection.prototype.handle = function (ctx, next) {
    var uc = this,
        applyOriginal = function(){
            // handover to original handle function from the UserCollection
            _handle.call(this, ctx, next);
        }.bind(uc);
        
    if(ctx.method !== 'PUT') {
        return applyOriginal();
    } else if (ctx.session && ctx.session.isRoot || ctx.req.internal){
        debug('ignoring internal or root request');
        return applyOriginal();
    }

    // set id if one wasnt provided in the query
    ctx.query.id = ctx.query.id || uc.parseId(ctx) || (ctx.body && ctx.body.id);

    var body = ctx.body;

    // old password check only applies if the user is trying to change his/her own password
    var isSelf = ctx.session.user && ctx.session.user.id === ctx.query.id || (ctx.body && ctx.body.id);
    if(body && body.password && isSelf){
        var oldPassword = body.oldPassword;
        delete body.oldPassword; // do not write to db!
        
        if(!oldPassword) {
            debug('old password not in request body');
            return ctx.done({errors: {oldPassword: 'is required'}});
        }

        // retrieve user manually, because the user from the session does not have the password available
        uc.store.first({id: ctx.session.user.id, $fields: {password: 1, username: 1}}, function(err, user){
            if(err) {
                debug('there was an error while searching for the current user: %j', err);
                return applyOriginal();
            }

            if(!uc.checkHash(uc, user, {password: oldPassword})) {
                debug('old password did not match with current!');
                return ctx.done({errors: {oldPassword: 'does not match current password'}});
            } else {
                debug('old password is valid!');
                return applyOriginal();
            }
        });
    } else {
        debug('put request does not try to update the user or is not updating himself, handing over to original user collection');
        return applyOriginal();
    }
};